import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
    
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title:Center(
        child: Text('My Flutter Demo'),
        ) 
     ),
     body: new MyList()

   );
  }
}


class MyList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
   return new ListView.builder(
     padding: const EdgeInsets.all(4.0),
     itemBuilder: (context,i){
       return new ListTile(
         title: new Text('Mr X'),
         subtitle: new Text('Online',style:new TextStyle(fontStyle:FontStyle.italic,color:Colors.green ),),
         leading: const Icon(Icons.face),
         trailing: new RaisedButton(
           child: new Text('Remove'),
           onPressed:(){
            deleteDialog(context).then((value){
              print('value is $value');

            });
           },
         ));
       },
    );
  }
}

Future<bool> deleteDialog(BuildContext context){

  return showDialog(
    context: context,
    builder: (BuildContext contxt){

   return new AlertDialog(
     title: new Text('Are You Sure'),
     actions: <Widget>[
      
         new FlatButton(
        child: new Text('yes'),
        onPressed: (){
         Navigator.of(context).pop(true); 
         },
        ),
      
      new FlatButton(
        child: new Text('no'),
        onPressed: (){
         Navigator.of(context).pop(false); 
         },
        )


     ],

   );

    }

  );



}